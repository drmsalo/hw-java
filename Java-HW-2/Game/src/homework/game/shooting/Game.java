package homework.game.shooting;

import java.util.Random;
import java.util.Scanner;

public class Game {
    public static char shootingArea(int[] userNum, int[] shipNum) {
        for (int i = 0; i < 2; i++) {
            if (userNum[i] != shipNum[i]) {
                return '-';
            }
        }
        return 'X';
    }

    public static int checkError(Scanner sc) {
        while (true) {
            System.out.println("Pick a number from 1 - 5");
            if (sc.hasNextInt()) {
                int pos = sc.nextInt();
                if (pos >= 1 && pos <= 5) {
                    return pos - 1;
                } else {
                    System.out.println("Put a number between 1 - 5");
                }
            } else {
                System.out.println("Put a valid Number");
                sc.next();
            }
        }
    }

    public static void main(String[] args) {
        final int SIZE = 5;
        int[] userNum = new int[2];
        int[] shipNum = new int[2];
        char[][] board = new char[SIZE][SIZE];
        Random rn = new Random();

        for (int i = 0; i < 2; i++) {
            shipNum[i] = rn.nextInt(5);
        }

        Scanner sc = new Scanner(System.in);

        System.out.println("All Set. Get ready to rumble!");



        while (true) {
            userNum[0] = checkError(sc);
            userNum[1] = checkError(sc);
            char val = shootingArea(userNum, shipNum);
            board[userNum[0]][userNum[1]] = val;

            for (int y = 0; y <= SIZE; y++) {
                System.out.print(y + " | ");
            }
            System.out.println();
            for (int y = 0; y < SIZE; y++) {
                System.out.print((y + 1) + " |");
                for (int x = 0; x < SIZE; x++) {
                    System.out.print(" " + board[y][x] + " |");
                }
                System.out.println();
            }

            if (val == 'X') {
                System.out.println("Congratulations, you won!");
                break;
            }
        }
    }
}