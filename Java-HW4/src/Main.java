public class Main {

    public static void main(String[] args) {

        Human anna = new Human("Anna","Walkman", 1999);
        Human mark = new Human("Mark","Walkman", 1998);
        Pet joj = new Pet("Cat", "John", 23, 95, new String[]{"sleep, drink"});
        Human Jess = new Human("Jessica", "Walker", 2008, 50, joj,  anna, mark, new String[][]{
                {"Sunday", "sport"},
                {"Monday", "work"},
                {"Tuesday", "gym"},
                {"Wednesday", "school"},
                {"Thursday", "shopping"},
                {"Friday", "movie"},
                {"Saturday", "drawing"}

        });
        Human tom = new Human("Tom", "Walker", 2018, 110, joj, anna, mark, new String[][]{
                {"Sunday", "play in the park"},
                {"Monday", "visit grandparents"},
                {"Tuesday", "reading"},
                {"Wednesday", "doctor's appointment"},
                {"Thursday", "playing PC games"},
                {"Friday", "meeting with friends"},
                {"Saturday", "go for a walk with the parents"}
        });

        Family family1 = new Family(anna, mark);

        family1.setPet(joj);
        System.out.println(family1);
        Jess.showMother();
        Jess.showFather();
        Jess.describePet();
    }
}
