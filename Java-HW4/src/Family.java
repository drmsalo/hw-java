import java.util.Arrays;
public class Family {
private static  final int MIN_FAM_LENGTH = 2;
private Human mother;
private Human father;
private Human[] children = new Human[0];
private Pet pet;

public Family(Human mother, Human father){
    this.mother = mother;
    this.father = father;
}
public void addChild(Human child){
Human[] newChildren = new Human[children.length + 1];
   newChildren[children.length] = child;
   children = newChildren;
}
public boolean abort(int index){
    if(index >= 0 && index < children.length){
        Human[] newChildren = new Human[children.length -1];
        int newIndex = 0;
        for(int i = 0; i< children.length; i++){
            if(i != index){
                newChildren[newIndex] = children[i];
                newIndex++;
            }
        }
        children = newChildren;
        return true;
    }else return false;
}

public int countFamily(){
    return MIN_FAM_LENGTH + children.length;
}
public void setChildren(Human [] children){
    this.children = children;
}

public Human getMother(){
    return mother;
}

    public void setMother(Human mother){
        this.mother = mother;
    }

public Human getFather(){
    return father;
}


public void setFather(Human father){
    this.father = father;
}


public Pet getPet(){
    return pet;
}

public void setPet(Pet pet){
    this.pet = pet;
}

public Human[] getChildren(){
    return children;
}

public void setChildren(){
    this.children = children;
}
    public String toString() {
        return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}", mother.toString(), father.toString(), Arrays.toString(children), pet);
    }

}
