import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;

    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] scedule;

    public Human(String name, String surname){
        this.name = name;
        this.surname = surname;
    }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] scedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.scedule = scedule;
    }
    public String getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }

    public void printName() {
        System.out.println(getName());
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname() {
        this.surname = surname;
    }

    public void printSurname() {
        System.out.println(getSurname());
    }

    public int getYear() {
        return year;
    }

    public void setYear() {
        this.year = year;
    }

    public void printYear() {
        System.out.println(getYear());
    }

    public int getIq() {
        return iq;
    }

    public void setIq() {
        this.iq = iq;
    }

    public void printIq() {
        System.out.println(getIq());
    }


    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void printPet() {
        System.out.println(getPet());
    }

    public Human getMother() {
        return mother;
    }
    public void setMother(){
        this.mother = mother;
    }
    public void printMother(){
        System.out.println(getMother());
    }

    public Human getFather(){
        return father;
    }
    public void setFather(){
        this.father = father;
    }
    public void printFather(){
        System.out.println(getFather());
    }

    public String[][] getScedule() {
        return scedule;
    }
    public void setScedule(){
        this.scedule = scedule;
    }

    public void printScedule(){
        System.out.println(getScedule());
    }


    public void showMother(){
        System.out.println(mother.getName());
    }
    public void showFather(){
        System.out.println(father.getName());
    }
    public void greetPet(){
        System.out.println("Hi "+ pet.getNickname());
    }

    public String toString() {
        return String.format("Human{name='%s', surname='%s'  year=%s, iq=%d}", name, surname, year, iq, mother, father, pet);
    }
    public String trickLvl;
    public void describePet(){
        if ((pet.getTrickLevel()) >= 50){
            trickLvl = "tricky";
        } else if (pet.getTrickLevel() < 50) {
            trickLvl = "not tricky";
        };
        System.out.println("I have " + pet.getSpecies() + " it is " + pet.getAge() + " years old and he is " + trickLvl);
    }

}
