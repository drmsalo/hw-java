import java.util.Arrays;
public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;



    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {

        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public Pet() {}

    public String getSpecies() {
        return species;
    }

    public void setSpecies() {
        this.species = species;
    }

    public void printSpecies() {
        System.out.println(getSpecies());
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname() {
        this.nickname = nickname;
    }

    public void printNickname() {
        System.out.println(getNickname());
    }

    public int getAge() {
        return age;
    }

    public void setAge() {
        this.age = age;
    }

    public void printAge() {
        System.out.println(getAge());
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel() {
        this.trickLevel = trickLevel;

    }

    public void printTrickLevel() {
        System.out.println(getTrickLevel());
    }


    public String[] getHabits() {
        return habits;
    }

    public void setHabits() {
        this.habits = habits;
    }

    public void printHabits() {
        System.out.println(getHabits());
    }

    public void eat(){
        System.out.println("I'm eating");
    }
    public void respond(){
        System.out.println("Hi boss, I'm " + nickname+ " I missed you" );
    }
    public void foul(){
        System.out.println("I need to hide the evidence...");
    }

    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", species, nickname, age, trickLevel, Arrays.toString(habits));
    }

}

